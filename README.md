# Bitcoin Cash adapter library

Bitcoin Cash adapter for [Timechain Lib Cryptos](https://bitbucket.org/timechain/tc-lib-cryptos).

## Unit tests

In a terminal, run:

```sh
npm run test
```

To be able to perform transactions, you must set some environment variables.

```bash
# The WIF of the wallet to use to send transactions
export WALLET_WIF="Kx2StqfkxHaEFFfXNTpVkESxKr3aTbiVu1jSRCYP9KVEsZm14R9g"

# The recipient address to send transactions
export RECIPIENT_ADDRESS="1MPsDH3sUD6xi5YWTbob5WARSKoFreKqbJ"

# Set no if you don't want to perform transactions in the tests
export PERFORM_TRANSACTIONS=true

```

## Web support

As the library works with [bitcoincashjs](https://github.com/bitcoincashjs/bitcoincashjs) that requires the Node.js `crypto` module, it cannot be used as-is.

Install the `crypto-browserify` module to have access to the `crypto` functions in web.

```bash
$ npm install --save crypto-browserify
```

Edit your Webpack configuration and use an alias to replace `crypto` by `crypto-browserify`.

```js
{
  resolve: {
    alias: {
      crypto: path.resolve(__dirname, 'node_modules/crypto-browserify')
    }
  }
}
```