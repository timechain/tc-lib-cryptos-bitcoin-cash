import { BigNumber } from 'bignumber.js'

import { BitcoinCashAdapter } from '../src'
import { CRYPTO } from '../src/utils/constants'
import { Wallet } from 'tc-lib-cryptos/dist/models'
import { BitcoinCashWallet } from '../src/models/wallet'

const TEST_ADDRESS = '1FbBbv5oYqFKwiPm4CAqvAy8345n8AQ74b'

async function environmentWallet (): Promise<BitcoinCashWallet> {
  return BitcoinCashAdapter.importWalletWithSeed(process.env.WALLET_WIF!)
}

test('Check environment variables', () => {
  expect(process.env.WALLET_WIF).toBeDefined()
  expect(process.env.RECIPIENT_ADDRESS).toBeDefined()
  expect(process.env.PERFORM_TRANSACTIONS).toBeDefined()
})

test('Get contract info', async () => {
  expect(BitcoinCashAdapter.name).toBe('Bitcoin Cash')
  expect(BitcoinCashAdapter.crypto).toBe('BCH')
  expect(BitcoinCashAdapter.decimals).toBe(8)
})

test('Generate random wallet', async () => {
  const { wallet } = await BitcoinCashAdapter.generateRandomWallet()
  expect(BitcoinCashAdapter.validateAddress(wallet.address, null)).toEqual(true)
  // console.log(wallet)
})

test('Import wallet', async () => {
  const wallet = await BitcoinCashAdapter.importWalletWithSeed('Kyr972iuBFh4yBzAziRDkh1V9LbvfdWePTm7Bj2sZzWra3bZDHHc')
  expect(BitcoinCashAdapter.validateAddress(wallet.address, null)).toEqual(true)
  expect(wallet.address).toEqual('14xUzmMWkfLH8GWJBxh8Uwnai7YRZEtPxu')
  expect(wallet.cashAddress).toEqual('bitcoincash:qq4kdgq948xgqlc6a4crlm7u6y3f0nf9pycr3e4yqn')
  expect(BitcoinCashAdapter.importWallet({})).rejects.toBeDefined()
  expect(BitcoinCashAdapter.importWallet({ privateKey: 'Kyr972iuBFh4yBzAziRDkh1V9LbvfdWePTm7Bj2sZzWra3bZDHHc' })).resolves.toBeDefined()
})

test('Validate address', () => {
  expect(BitcoinCashAdapter.validateAddress(TEST_ADDRESS, null)).toEqual(true)
  expect(BitcoinCashAdapter.validateAddress('abc', null)).toEqual(false)
  expect(BitcoinCashAdapter.validateAddress('qzsq3s8k6cpkd9r35y7pezssrf4pcuayavs4ne7ecc', null)).toEqual(true)
  expect(BitcoinCashAdapter.validateAddress('bitcoincash:qpm2qsznhks23z7629mms6s4cwef74vcwvy22gdx6a', null)).toEqual(true)
})

test('Get balance', async () => {
  const balance = await BitcoinCashAdapter.getBalance({
    address: TEST_ADDRESS
  })
  expect(balance).toBeDefined()
})

test('Get transactions', async () => {
  const transactions = await BitcoinCashAdapter.getTransactions({
    address: TEST_ADDRESS
  })
  expect(transactions.length).toBeGreaterThan(0)
})

test('Estimate transactions fees', async () => {
  const wallet = (await BitcoinCashAdapter.generateRandomWallet()).wallet
  wallet.address = TEST_ADDRESS

  const fees = await BitcoinCashAdapter.getTransactionFees({
    wallet: wallet,
    recipientAddress: TEST_ADDRESS,
    amount: new BigNumber(12345),
    fees: 'minimum'
  })
  expect(fees.toNumber()).toBeGreaterThan(0)
})

if (process.env.PERFORM_TRANSACTIONS === 'true' || process.env.PERFORM_TRANSACTIONS === '1') {
  test('Send transaction', async () => {
    const wallet = await environmentWallet()
    const transaction = await BitcoinCashAdapter.sendTransaction({
      wallet: wallet,
      recipientAddress: process.env.RECIPIENT_ADDRESS!,
      amount: new BigNumber(12345),
      fees: 'minimum'
    })
    expect(transaction.hash).toBeDefined()
  })
}
