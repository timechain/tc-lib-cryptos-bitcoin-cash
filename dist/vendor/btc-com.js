"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var bignumber_js_1 = require("bignumber.js");
var url_1 = require("../utils/url");
var constants_1 = require("../utils/constants");
var BASE_URL = 'https://bch-chain.api.btc.com/v3';
function url(endpoint) {
    return url_1.buildUrl(BASE_URL, endpoint);
}
/**
 * Gets the total balance of an address
 * @param address The address
 */
function getBalance(address) {
    return __awaiter(this, void 0, void 0, function () {
        var data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, axios_1.default.get(url("address/" + address))];
                case 1:
                    data = (_a.sent()).data;
                    return [2 /*return*/, new bignumber_js_1.BigNumber(data.data.balance)];
            }
        });
    });
}
exports.getBalance = getBalance;
/**
 * Fetch all transactions related to an address
 * @param address The address
 */
function getRawTransactions(address) {
    return __awaiter(this, void 0, void 0, function () {
        var data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, axios_1.default.get(url("address/" + address + "/tx"))];
                case 1:
                    data = (_a.sent()).data;
                    if (data.data && data.data.list)
                        return [2 /*return*/, data.data.list];
                    return [2 /*return*/, []];
            }
        });
    });
}
exports.getRawTransactions = getRawTransactions;
/**
 * Splits a raw transaction to a list of transactions for every output
 * @param tx The raw transaction
 * @param address The address related to the transaction, to know wich input / output to choose.
 * @param ignoreCashbackOutputs Ignore outputs with same address as input, probably used for cashback
 */
function rawTxToTransactions(tx, address, ignoreCashbackOutputs) {
    var transactions = [];
    var isSender = (tx.balance_diff < 0);
    var sender = (isSender) ? address : tx.inputs[0].prev_addresses[0];
    for (var _i = 0, _a = tx.outputs; _i < _a.length; _i++) {
        var out = _a[_i];
        if (ignoreCashbackOutputs) {
            if (isSender && isSameAddress(address, out.addresses[0]))
                continue;
        }
        // If address if recipient, ignore outputs unrelated to the address
        if (!isSender && !isSameAddress(out.addresses[0], address))
            continue;
        transactions.push({
            hash: tx.hash,
            date: new Date(tx.created_at * 1000),
            fromAddress: sender,
            toAddress: out.addresses[0],
            amount: new bignumber_js_1.BigNumber(out.value),
            crypto: constants_1.CRYPTO
        });
    }
    return transactions;
}
exports.rawTxToTransactions = rawTxToTransactions;
/**
 * Returns the list of unspent transaction outputs
 * @param address The address
 */
function getUnspentTransactions(address) {
    return __awaiter(this, void 0, void 0, function () {
        var data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, axios_1.default.get(url("address/" + address + "/unspent"))];
                case 1:
                    data = (_a.sent()).data;
                    if (data.data && data.data.list)
                        return [2 /*return*/, data.data.list];
                    return [2 /*return*/, []];
            }
        });
    });
}
exports.getUnspentTransactions = getUnspentTransactions;
function sendTransaction(rawTransactionHex) {
    return __awaiter(this, void 0, void 0, function () {
        var url, data;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    url = 'https://bch.btc.com/tools/tx-publish';
                    return [4 /*yield*/, axios_1.default.post(url, {
                            rawhex: rawTransactionHex
                        })];
                case 1:
                    data = (_a.sent()).data;
                    if (data.err_no)
                        throw new Error(data.err_msg || 'Could not send transaction');
                    return [2 /*return*/];
            }
        });
    });
}
exports.sendTransaction = sendTransaction;
function isSameAddress(address1, address2) {
    return (!!address1 && !!address2 && address1 === address2);
}
