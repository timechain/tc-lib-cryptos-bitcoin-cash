import { BigNumber } from 'bignumber.js';
import { Transaction } from 'tc-lib-cryptos/dist/models';
export interface Response<T> {
    err_no: number;
    err_msg: string;
    data: T;
}
export declare type AddressResponse = Response<{
    address: string;
    received: number;
    sent: number;
    balance: number;
    tx_count: number;
    unconfirmed_tx_count: number;
    unconfirmed_received: number;
    unconfirmed_sent: number;
    unspent_tx_count: number;
    first_tx: string;
    last_tx: string;
}>;
export interface RawTransaction {
    confirmations: number;
    block_height: number;
    block_hash: string;
    block_time: number;
    created_at: number;
    fee: number;
    hash: string;
    inputs_count: number;
    inputs_value: number;
    is_coinbase: boolean;
    is_double_spend: boolean;
    is_sw_tx: boolean;
    weight: number;
    vsize: number;
    witness_hash: string;
    lock_time: number;
    outputs_count: number;
    outputs_value: number;
    size: number;
    sigops: number;
    version: number;
    inputs: {
        prev_addresses: string[];
        prev_position: number;
        prev_tx_hash: string;
        prev_type: string;
        prev_value: number;
        sequence: any;
    }[];
    outputs: {
        addresses: string[];
        value: number;
        type: string;
        spent_by_tx: string;
        spent_by_tx_position: number;
    }[];
    balance_diff: number;
}
export declare type TransactionsResponse = Response<{
    total_count: number;
    page: number;
    pagesize: number;
    list: RawTransaction[];
}>;
export interface UTXO {
    tx_hash: string;
    tx_output_n: number;
    tx_output_n2: number;
    value: number;
    confirmations: number;
}
export declare type UTXOResponse = Response<{
    total_count: number;
    page: number;
    pagesize: number;
    list: UTXO[];
}>;
/**
 * Gets the total balance of an address
 * @param address The address
 */
export declare function getBalance(address: string): Promise<BigNumber>;
/**
 * Fetch all transactions related to an address
 * @param address The address
 */
export declare function getRawTransactions(address: string): Promise<RawTransaction[]>;
/**
 * Splits a raw transaction to a list of transactions for every output
 * @param tx The raw transaction
 * @param address The address related to the transaction, to know wich input / output to choose.
 * @param ignoreCashbackOutputs Ignore outputs with same address as input, probably used for cashback
 */
export declare function rawTxToTransactions(tx: RawTransaction, address: string, ignoreCashbackOutputs: boolean): Transaction[];
/**
 * Returns the list of unspent transaction outputs
 * @param address The address
 */
export declare function getUnspentTransactions(address: string): Promise<UTXO[]>;
export declare function sendTransaction(rawTransactionHex: string): Promise<void>;
