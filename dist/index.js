"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var bignumber_js_1 = require("bignumber.js");
var bch = require("bitcoincashjs");
var bchaddr = require("bchaddrjs");
var btc_com_1 = require("./vendor/btc-com");
var constants_1 = require("./utils/constants");
var BitcoinAdapterClass = /** @class */ (function () {
    function BitcoinAdapterClass() {
        this.crypto = constants_1.CRYPTO;
        this.name = 'Bitcoin Cash';
        this.decimals = 8;
    }
    BitcoinAdapterClass.prototype.generateRandomWallet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var wallet;
            return __generator(this, function (_a) {
                wallet = this.importWalletWithPrivateKey(new bch.PrivateKey());
                return [2 /*return*/, {
                        wallet: wallet,
                        seed: wallet.privateKey
                    }];
            });
        });
    };
    BitcoinAdapterClass.prototype.importWalletWithSeed = function (seed) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.importWalletWithPrivateKey(new bch.PrivateKey(seed))];
            });
        });
    };
    BitcoinAdapterClass.prototype.importWallet = function (info) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!info.privateKey)
                    throw new Error('Private key (seed) not provided.');
                // Consider the seed as the private key
                return [2 /*return*/, this.importWalletWithSeed(info.privateKey)];
            });
        });
    };
    BitcoinAdapterClass.prototype.importWalletWithPrivateKey = function (privateKey) {
        var pk = privateKey;
        var wif = pk.toWIF();
        var address = pk.toAddress().toString();
        return {
            address: address,
            privateKey: wif,
            publicKey: pk.toPublicKey().toString(),
            crypto: constants_1.CRYPTO,
            cashAddress: bchaddr.toCashAddress(address)
        };
    };
    BitcoinAdapterClass.prototype.getBalance = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                if (!wallet.address)
                    throw new Error('No wallet address was specified');
                return [2 /*return*/, btc_com_1.getBalance(wallet.address)];
            });
        });
    };
    BitcoinAdapterClass.prototype.sendTransaction = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var transaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildTransaction({
                            wallet: options.wallet,
                            recipientAddress: options.recipientAddress,
                            amount: options.amount
                        })];
                    case 1:
                        transaction = _a.sent();
                        transaction.sign(new bch.PrivateKey(options.wallet.privateKey));
                        return [4 /*yield*/, btc_com_1.sendTransaction(transaction.toString())];
                    case 2:
                        _a.sent();
                        return [2 /*return*/, {
                                hash: transaction.hash,
                                date: new Date(),
                                fromAddress: options.wallet.address,
                                toAddress: options.recipientAddress,
                                amount: options.amount,
                                crypto: constants_1.CRYPTO
                            }];
                }
            });
        });
    };
    BitcoinAdapterClass.prototype.getTransactionFees = function (params) {
        return __awaiter(this, void 0, void 0, function () {
            var transaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.buildTransaction({
                            wallet: params.wallet,
                            recipientAddress: params.recipientAddress,
                            amount: params.amount
                        })];
                    case 1:
                        transaction = _a.sent();
                        return [2 /*return*/, new bignumber_js_1.BigNumber(transaction.getFee())];
                }
            });
        });
    };
    BitcoinAdapterClass.prototype.getTransactions = function (wallet) {
        return __awaiter(this, void 0, void 0, function () {
            var rawTransactions, transactions, _i, rawTransactions_1, rawTx, _a, _b, tx;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        if (!wallet.address)
                            throw new Error('No wallet address was specified');
                        return [4 /*yield*/, btc_com_1.getRawTransactions(wallet.address)];
                    case 1:
                        rawTransactions = _c.sent();
                        transactions = [];
                        for (_i = 0, rawTransactions_1 = rawTransactions; _i < rawTransactions_1.length; _i++) {
                            rawTx = rawTransactions_1[_i];
                            for (_a = 0, _b = btc_com_1.rawTxToTransactions(rawTx, wallet.address, true); _a < _b.length; _a++) {
                                tx = _b[_a];
                                transactions.push(tx);
                            }
                        }
                        return [2 /*return*/, transactions];
                }
            });
        });
    };
    BitcoinAdapterClass.prototype.validateAddress = function (address, options) {
        try {
            return bchaddr.isLegacyAddress(address) || bchaddr.isCashAddress(address);
        }
        catch (error) {
            return false;
        }
    };
    /**
     * Returns the list of unspent transactions outputs of an address
     * @param address The address
     */
    BitcoinAdapterClass.prototype.utxo = function (address) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, btc_com_1.getUnspentTransactions(address)];
            });
        });
    };
    BitcoinAdapterClass.prototype.buildTransaction = function (options) {
        return __awaiter(this, void 0, void 0, function () {
            var address, utxos, rawUTXOs, _i, rawUTXOs_1, utxo, transaction;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        address = new bch.Address(options.wallet.address);
                        utxos = [];
                        return [4 /*yield*/, this.utxo(options.wallet.address)];
                    case 1:
                        rawUTXOs = _a.sent();
                        if (!rawUTXOs || rawUTXOs.length === 0) {
                            throw new Error('Could not build transaction. Does the wallet have enough crypto?');
                        }
                        for (_i = 0, rawUTXOs_1 = rawUTXOs; _i < rawUTXOs_1.length; _i++) {
                            utxo = rawUTXOs_1[_i];
                            utxos.push({
                                txId: utxo.tx_hash,
                                outputIndex: utxo.tx_output_n,
                                address: options.wallet.address,
                                script: new bch.Script(address).toHex(),
                                satoshis: utxo.value
                            });
                        }
                        transaction = new bch.Transaction()
                            .from(utxos)
                            .to(options.recipientAddress, options.amount.toNumber())
                            .change(options.wallet.address);
                        if (options.feeRate)
                            transaction.feePerKb(options.feeRate);
                        return [2 /*return*/, transaction];
                }
            });
        });
    };
    return BitcoinAdapterClass;
}());
exports.BitcoinAdapterClass = BitcoinAdapterClass;
exports.BitcoinCashAdapter = new BitcoinAdapterClass();
