import { BigNumber } from 'bignumber.js';
import { Wallet, Transaction } from 'tc-lib-cryptos/dist/models';
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet';
import * as bch from 'bitcoincashjs';
import { UTXO } from './vendor/btc-com';
import { BitcoinCashWallet } from './models/wallet';
export declare class BitcoinAdapterClass implements WalletAdapter<BitcoinCashWallet> {
    crypto: string;
    name: string;
    decimals: number;
    generateRandomWallet(): Promise<{
        wallet: BitcoinCashWallet;
        seed: string;
    }>;
    importWalletWithSeed(seed: string): Promise<BitcoinCashWallet>;
    importWallet(info: {
        publicKey?: string;
        privateKey?: string;
        address?: string;
    }): Promise<BitcoinCashWallet>;
    importWalletWithPrivateKey(privateKey: bch.PrivateKey): BitcoinCashWallet;
    getBalance(wallet: Partial<BitcoinCashWallet>): Promise<BigNumber>;
    sendTransaction(options: SendTransactionOptions<Wallet>): Promise<Transaction>;
    getTransactionFees(params: SendTransactionOptions<Wallet>): Promise<BigNumber>;
    getTransactions(wallet: Partial<BitcoinCashWallet>): Promise<Transaction[]>;
    validateAddress(address: string, options: object | null): boolean;
    /**
     * Returns the list of unspent transactions outputs of an address
     * @param address The address
     */
    utxo(address: string): Promise<UTXO[]>;
    private buildTransaction(options);
}
export declare const BitcoinCashAdapter: BitcoinAdapterClass;
