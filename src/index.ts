import { BigNumber } from 'bignumber.js'
import { Wallet, Transaction } from 'tc-lib-cryptos/dist/models'
import { WalletAdapter, SendTransactionOptions } from 'tc-lib-cryptos/dist/modules/wallet'

import * as bch from 'bitcoincashjs'
import * as bchaddr from 'bchaddrjs'
import { getBalance, getRawTransactions, rawTxToTransactions, UTXO, getUnspentTransactions, sendTransaction } from './vendor/btc-com'
import { CRYPTO } from './utils/constants'
import { BitcoinCashWallet } from './models/wallet'

export class BitcoinAdapterClass implements WalletAdapter<BitcoinCashWallet> {
  crypto = CRYPTO

  name = 'Bitcoin Cash'

  decimals = 8

  async generateRandomWallet (): Promise<{
    wallet: BitcoinCashWallet,
    seed: string
  }> {
    const wallet = this.importWalletWithPrivateKey(new bch.PrivateKey())
    return {
      wallet: wallet,
      seed: wallet.privateKey
    }
  }

  async importWalletWithSeed (seed: string): Promise<BitcoinCashWallet> {
    return this.importWalletWithPrivateKey(new bch.PrivateKey(seed))
  }

  async importWallet (info: {
    publicKey?: string;
    privateKey?: string;
    address?: string;
  }): Promise<BitcoinCashWallet> {
    if (!info.privateKey) throw new Error('Private key (seed) not provided.')
    // Consider the seed as the private key
    return this.importWalletWithSeed(info.privateKey)
  }

  importWalletWithPrivateKey (privateKey: bch.PrivateKey): BitcoinCashWallet {
    let pk: bch.PrivateKey = privateKey
    let wif: string = pk.toWIF()
    const address = pk.toAddress().toString()
    return {
      address: address,
      privateKey: wif,
      publicKey: pk.toPublicKey().toString(),
      crypto: CRYPTO,
      cashAddress: bchaddr.toCashAddress(address)
    }
  }

  async getBalance (wallet: Partial<BitcoinCashWallet>): Promise<BigNumber> {
    if (!wallet.address) throw new Error('No wallet address was specified')
    return getBalance(wallet.address)
  }

  async sendTransaction (options: SendTransactionOptions<Wallet>): Promise<Transaction> {
    const transaction = await this.buildTransaction({
      wallet: options.wallet,
      recipientAddress: options.recipientAddress,
      amount: options.amount
    })
    transaction.sign(new bch.PrivateKey(options.wallet.privateKey))
    await sendTransaction(transaction.toString())

    return {
      hash: transaction.hash,
      date: new Date(),
      fromAddress: options.wallet.address,
      toAddress: options.recipientAddress,
      amount: options.amount,
      crypto: CRYPTO
    }
  }

  async getTransactionFees (params: SendTransactionOptions<Wallet>): Promise<BigNumber> {
    const transaction = await this.buildTransaction({
      wallet: params.wallet,
      recipientAddress: params.recipientAddress,
      amount: params.amount
    })
    return new BigNumber(transaction.getFee())
  }

  async getTransactions (wallet: Partial<BitcoinCashWallet>): Promise<Transaction[]> {
    if (!wallet.address) throw new Error('No wallet address was specified')
    const rawTransactions = await getRawTransactions(wallet.address)
    const transactions: Transaction[] = []
    for (const rawTx of rawTransactions) {
      for (const tx of rawTxToTransactions(rawTx, wallet.address, true)) {
        transactions.push(tx)
      }
    }
    return transactions
  }

  validateAddress (address: string, options: object | null): boolean {
    try {
      return bchaddr.isLegacyAddress(address) || bchaddr.isCashAddress(address)
    } catch (error) {
      return false
    }
  }

  /**
   * Returns the list of unspent transactions outputs of an address
   * @param address The address
   */
  async utxo (address: string): Promise<UTXO[]> {
    return getUnspentTransactions(address)
  }

  private async buildTransaction (options: {
    wallet: Wallet
    recipientAddress: string
    amount: BigNumber
    /**
     * Fee rate in satoshi per KB
     */
    feeRate?: BigNumber
  }): Promise<bch.Transaction> {
    const address = new bch.Address(options.wallet.address)

    /*
     * Fetch UTXOs and convert them for coinsupport library
     */
    const utxos: {
      txId: string,
      outputIndex: number,
      address: string,
      script: string,
      satoshis: number
    }[] = []
    const rawUTXOs = await this.utxo(options.wallet.address)

    if (!rawUTXOs || rawUTXOs.length === 0) {
      throw new Error('Could not build transaction. Does the wallet have enough crypto?')
    }

    for (const utxo of rawUTXOs) {
      utxos.push({
        txId: utxo.tx_hash,
        outputIndex: utxo.tx_output_n,
        address: options.wallet.address,
        script: new bch.Script(address).toHex(),
        satoshis: utxo.value
      })
    }

    // Create a transaction with the result inputs and outputs
    const transaction = new bch.Transaction()
    .from(utxos)
    .to(options.recipientAddress, options.amount.toNumber())
    .change(options.wallet.address)

    if (options.feeRate) transaction.feePerKb(options.feeRate)

    return transaction
  }
}

export const BitcoinCashAdapter = new BitcoinAdapterClass()
