import axios from 'axios'
import { BigNumber } from 'bignumber.js'

import { buildUrl } from '../utils/url'
import { CRYPTO } from '../utils/constants'
import { Transaction } from 'tc-lib-cryptos/dist/models'

const BASE_URL = 'https://bch-chain.api.btc.com/v3'

export interface Response<T> {
  err_no: number
  err_msg: string
  data: T
}

export type AddressResponse = Response<{
  address: string
  received: number
  sent: number
  balance: number
  tx_count: number
  unconfirmed_tx_count: number
  unconfirmed_received: number
  unconfirmed_sent: number
  unspent_tx_count: number
  first_tx: string
  last_tx: string
}>

export interface RawTransaction {
  confirmations: number
  block_height: number
  block_hash: string
  block_time: number
  created_at: number
  fee: number
  hash: string
  inputs_count: number
  inputs_value: number
  is_coinbase: boolean
  is_double_spend: boolean
  is_sw_tx: boolean
  weight: number
  vsize: number
  witness_hash: string
  lock_time: number
  outputs_count: number
  outputs_value: number
  size: number
  sigops: number
  version: number
  inputs: {
    prev_addresses: string[]
    prev_position: number
    prev_tx_hash: string
    prev_type: string
    prev_value: number
    sequence: any
  }[]
  outputs: {
    addresses: string[]
    value: number
    type: string
    spent_by_tx: string
    spent_by_tx_position: number
  }[]
  balance_diff: number
}

export type TransactionsResponse = Response<{
  total_count: number
  page: number
  pagesize: number
  list: RawTransaction[]
}>

export interface UTXO {
  tx_hash: string
  tx_output_n: number
  tx_output_n2: number
  value: number
  confirmations: number
}

export type UTXOResponse = Response<{
  total_count: number
  page: number
  pagesize: number
  list: UTXO[]
}>

function url (endpoint: string) {
  return buildUrl(BASE_URL, endpoint)
}

/**
 * Gets the total balance of an address
 * @param address The address
 */
export async function getBalance (address: string): Promise<BigNumber> {
  const { data } = await axios.get<AddressResponse>(url(`address/${address}`))
  return new BigNumber(data.data.balance)
}

/**
 * Fetch all transactions related to an address
 * @param address The address
 */
export async function getRawTransactions (address: string): Promise<RawTransaction[]> {
  const { data } = await axios.get<TransactionsResponse>(url(`address/${address}/tx`))
  if (data.data && data.data.list) return data.data.list
  return []
}

/**
 * Splits a raw transaction to a list of transactions for every output
 * @param tx The raw transaction
 * @param address The address related to the transaction, to know wich input / output to choose.
 * @param ignoreCashbackOutputs Ignore outputs with same address as input, probably used for cashback
 */
export function rawTxToTransactions (tx: RawTransaction, address: string, ignoreCashbackOutputs: boolean): Transaction[] {
  const transactions: Transaction[] = []
  const isSender = (tx.balance_diff < 0)
  const sender = (isSender) ? address : tx.inputs[0].prev_addresses[0]

  for (const out of tx.outputs) {
    if (ignoreCashbackOutputs) {
      if (isSender && isSameAddress(address, out.addresses[0])) continue
    }

    // If address if recipient, ignore outputs unrelated to the address
    if (!isSender && !isSameAddress(out.addresses[0], address)) continue

    transactions.push({
      hash: tx.hash,
      date: new Date(tx.created_at * 1000),
      fromAddress: sender,
      toAddress: out.addresses[0],
      amount: new BigNumber(out.value),
      crypto: CRYPTO
    })
  }

  return transactions
}

/**
 * Returns the list of unspent transaction outputs
 * @param address The address
 */
export async function getUnspentTransactions (address: string): Promise<UTXO[]> {
  const { data } = await axios.get<UTXOResponse>(url(`address/${address}/unspent`))
  if (data.data && data.data.list) return data.data.list
  return []
}

export async function sendTransaction (rawTransactionHex: string): Promise<void> {
  const url = 'https://bch.btc.com/tools/tx-publish'
  const { data } = await axios.post<Response<{}>>(url, {
    rawhex: rawTransactionHex
  })
  if (data.err_no) throw new Error(data.err_msg || 'Could not send transaction')
}

function isSameAddress (address1: string, address2: string): boolean {
  return (!!address1 && !!address2 && address1 === address2)
}
