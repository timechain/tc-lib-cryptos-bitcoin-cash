import { Wallet } from 'tc-lib-cryptos/dist/models'

export interface BitcoinCashWallet extends Wallet {
  /**
   * Address in Bitcoin Cash address format.
   * Example: bitcoincash:qr0q67nsn66cf3klfufttr0vuswh3w5nt5jqpp20t9
   */
  cashAddress: string
}
